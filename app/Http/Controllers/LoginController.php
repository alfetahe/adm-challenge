<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
  public function office365() {
    return Socialite::driver('azure')->redirect();
  }

  public function office365Redirect() {
    $user = Socialite::driver('azure')->stateless()->user();
    $user = User::firstOrCreate([
      'email' => $user->email
    ], [
      'name' => $user->name,
      'password' => Hash::make(Str::random(24))
    ]);

    Auth::login($user, true);

    return redirect('/dashboard');
  }

  public function github() {
    return Socialite::driver('github')->redirect();
  }

  public function githubRedirect() {
    $user = Socialite::driver('github')->user();
  }
}
