<?php

namespace App\Http\Controllers;

use App\Models\Challenge;
use App\Models\ChallengeStatus;
use App\Models\ChallengeTarget;
use App\Models\User;
use Illuminate\Http\Request;

class ChallengeController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index() {
    $challenges = Challenge::all();
    return view('challenge.view.challenges', ['challenges' => $challenges]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
   */
  public function create() {
    $users = User::where('id', '!=', auth()->id())->get();
    $statuses = ChallengeStatus::STATUSES;

    return view('challenge.create-edit', ['route' => route('challenge.store'), 'users' => $users, 'statuses' => $statuses]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    $challenge = new Challenge;

    $challenge->challenge_title = $request->get('challenge_title');
    $challenge->challenge_description = $request->get('challenge_description');
    $challenge->user_id = auth()->user()->id;
    $challenge->deadline = $request->get('deadline');
    $challenge->bet = $request->get('bet');
    $challenge->save();

    if ($request->hasFile('image')) {
      $filename = time() . '.' . $request->file('image')->getClientOriginalExtension();
      $challenge->addMediaFromRequest('image')
        ->setFileName($filename)->toMediaCollection('challenge');
    }

    $challenge->targets()->updateOrCreate(
      [
        'challenge_id' => $challenge->id, 'user_id' => $request->get('targets')
      ]
    );

    $challenge->status()->updateOrCreate(
      [
        'challenger_target_id' => $challenge->id,
        'status_text_id' => (int)$request->get('status'),
      ]
    );

    return redirect(route('challenges'));
  }

  /**
   * Display the specified resource.
   *
   * @param \App\Models\Challenge $challenge
   * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
   */
  public function show(Challenge $challenge) {
    $statuses = ChallengeStatus::STATUSES;
    return view('challenge.view.challenge-detail',
      [
        'update' => route('challenge.update_status', ['challenge' => $challenge]),
        'route' => route('challenge.show', [
          'challenge' => $challenge]),
        'challenge' => $challenge->with('challenger')
          ->where('id', $challenge->id)->get()[0], 'statuses' => $statuses]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param \App\Models\Challenge $challenge
   * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
   */
  public function edit(Challenge $challenge) {
    $users = User::where('id', '!=', auth()->id())->get();
    $statuses = ChallengeStatus::STATUSES;

    return view('challenge.create-edit',
      [
        'route' => route('challenge.update', ['challenge' => $challenge]),
        'challenge' => $challenge, 'users' => $users, 'statuses' => $statuses]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param \Illuminate\Http\Request $request
   * @param \App\Models\Challenge $challenge
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Challenge $challenge) {
    $challenge->challenge_title = $request->get('challenge_title');
    $challenge->challenge_description = $request->get('challenge_description');
    $challenge->user_id = auth()->user()->id;
    $challenge->deadline = $request->get('deadline');
    $challenge->bet = $request->get('bet');
    $challenge->save();
    if ($request->hasFile('image')) {
      $challenge->media()->delete();
      $filename = time() . '.' . $request->file('image')->getClientOriginalExtension();
      $challenge->addMediaFromRequest('image')->setFileName($filename)->toMediaCollection('challenge');
    }

    $challenge->targets()->updateOrCreate(
      [
        'challenge_id' => $challenge->id, 'user_id' => $request->get('targets')
      ]
    );


    $challenge->status()->updateOrCreate(
      [
        'challenger_target_id' => $challenge->id,
        'status_text_id' => (int)$request->get('status'),
      ]
    );

    return redirect(route('challenges'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param \App\Models\Challenge $challenge
   * @return \Illuminate\Http\Response
   */
  public function destroy(Challenge $challenge) {
    //
  }

  public function dashboard() {
    $topChallengers = $this->getTopChallengers();

    $topTargets = $this->getTopTargets();

    return view('dashboard', ['top_challengers' => $topChallengers, 'top_targets' => $topTargets]);
  }

  protected function getTopChallengers() {
    $users = User::all();

    $topChallengers = [];
    foreach ($users as $user) {
      $count = Challenge::where('user_id', $user->id)->count();
      if ($count > 0 || 1 == 1) {
        $topChallengers[] = [
          'challenger' => $user->name,
          'challenges_count' => $count
        ];
      }
    }

    $challengers = array_column($topChallengers, 'challenges_count');

    array_multisort($challengers, SORT_DESC, $topChallengers);

    return $topChallengers;
  }

  protected function getTopTargets() {
    $users = User::all();

    $topTargets = [];
    foreach ($users as $user) {
      $count = ChallengeTarget::where('user_id', $user->id)->count();
      if ($count > 0 || 1 == 1) {
        $topTargets[] = [
          'target' => $user->name,
          'target_challenges_count' => $count
        ];
      }
    }

    $targets = array_column($topTargets, 'target_challenges_count');

    array_multisort($targets, SORT_DESC, $topTargets);

    return $topTargets;
  }

  public function updateStatus(Challenge $challenge, Request $request) {
    $new_status_id = $request->get('status');
    $challenge->status()->update(
      [
        'challenger_target_id' => $challenge->id,
        'status_text_id' => (int)$new_status_id,
      ]);
    if ($request->hasFile('image')) {
      $challenge->status->media()->delete();
      $filename = time() . '.' . $request->file('image')->getClientOriginalExtension();
      $challenge->status->addMediaFromRequest('image')->setFileName($filename)->toMediaCollection('challenge_status');
    }

    return redirect(route('challenges'));
  }

}
