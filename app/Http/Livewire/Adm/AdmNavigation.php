<?php

namespace App\Http\Livewire\Adm;

use Livewire\Component;

class AdmNavigation extends Component
{
    public function render()
    {
        return view('livewire.adm.adm-navigation');
    }
}
