<?php

namespace App\Http\Livewire\Adm;

use Livewire\Component;

class AdmLogo extends Component
{
    public function render()
    {
        return view('livewire.adm.adm-logo');
    }
}
