<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChallengeTarget extends Model
{
    use HasFactory;

    protected $fillable = [
      'challenge_id',
      'user_id'
    ];

    public function challenge() {
      $this->belongsTo(Challenge::class);
    }

    public function user() {
      return User::find($this->user_id);
    }

}
