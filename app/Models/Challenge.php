<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Challenge extends Model implements HasMedia
{
  use HasFactory;
  use InteractsWithMedia;

  protected $fillable = [
    'challenger_id',
    'challenge_description',
    'deadline',
    'challenge_title'
  ];

  public function targets() {
    return $this->hasMany(ChallengeTarget::class);
  }

  public function challenger() {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function status() {
    return $this->hasOne(ChallengeStatus::class, 'challenger_target_id');
  }

}
