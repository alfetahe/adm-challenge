<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class ChallengeStatus extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = ['challenger_target_id', 'status_text_id'];

    const STATUSES = [
      0 => ['text' => 'Pending', 'color' => 'yellow'],
      1 => ['text' => 'In progress', 'color' => 'yellow'],
      2 => ['text' => 'Declined','color' => 'red'],
      3 => ['text' => 'Won', 'color' => 'green']
    ];

    public function challengerTarget() {
      return $this->belongsTo(ChallengeTarget::class, 'challenger_target_id');
    }

}
