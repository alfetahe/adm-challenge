<?php

use App\Http\Controllers\ChallengeController;
use App\Http\Controllers\FileUploadController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PagesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/**
 * Office 365 login
 */

Route::get('/office', [LoginController::class, 'office365'])->name('login.microsoft');
Route::get('/github', [LoginController::class, 'github'])->name('login.github');
Route::get('/github/redirect', [LoginController::class, 'githubRedirect'])->name('login.github');
Route::get('/login/redirect', [LoginController::class, 'office365Redirect'])->name('redirect.microsoft');


Route::get('/', function () {
  return view('welcome');
});



/* CHALLENGE ROUTES */
Route::middleware(['auth:sanctum', 'verified'])->group(function () {

  Route::post('/challenge/update/{challenge}', [ChallengeController::class, 'updateStatus'])->name('challenge.update_status');

  Route::get('/challenge/create', [ChallengeController::class, 'create'])->name('challenge.create');

  Route::get('/challenge/{challenge}/edit', [ChallengeController::class, 'edit'])->name('challenge.edit');

  Route::post('/challenge/store', [ChallengeController::class, 'store'])->name('challenge.store');

  Route::get('/challenge/{challenge}', [ChallengeController::class, 'show'])
    ->name('challenge.show');

  Route::get('/challenges', [ChallengeController::class, 'index'])
    ->name('challenges');

  Route::post('/challenge/{challenge}/update', [ChallengeController::class, 'update'])->name('challenge.update');

  Route::get('/dashboard', [ChallengeController::class, 'dashboard'])
    ->name('dashboard');


  Route::get('/about', [PagesController::class, 'about'])
    ->name('about');
});
