<x-app-layout>
    <x-slot name="header">
        <h1 class="text-3xl font-extrabold tracking-tight text-white my-10">
            {{ __('The challenge') }}
        </h1>
    </x-slot>

    <div class="max-w-2xl">
        <div class="bg-white shadow overflow-hidden sm:rounded-lg">
            <div class="px-4 py-5 sm:px-6">
                <h3 class="text-lg leading-6 font-medium text-gray-900">
                    {{$challenge['challenge_title']}}
                </h3>
                <p class="mt-1 max-w-2xl text-sm text-gray-500">
                    Deadline: {{ date('d.m.Y', strtotime($challenge['deadline'])) }}
                </p>
                <p class="mt-1 max-w-2xl text-sm text-gray-500">
                    Status: {{ \App\Models\ChallengeStatus::STATUSES[$challenge->status()->first()->status_text_id]['text'] }}
                </p>
            </div>
            <div class="border-t border-gray-200">
                <dl>
                    <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt class="text-sm font-medium text-gray-500">
                            Challenger
                        </dt>
                        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                            {{ $challenge['challenger']['name'] }}
                        </dd>
                    </div>
                    <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt class="text-sm font-medium text-gray-500">
                            Challenge target
                        </dt>
                        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                            {{ $challenge->targets()->first()->user()->name }}
                        </dd>
                    </div>
                    <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt class="text-sm font-medium text-gray-500">
                            Challenge description
                        </dt>
                        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                            {{$challenge['challenge_description']}}
                        </dd>
                    </div>
                    <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                        <dt class="text-sm font-medium text-gray-500">
                            Bet
                        </dt>
                        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                            {{ $challenge['bet'] ?? '' }}
                        </dd>
                    </div>
                    @if($challenge->getMedia('challenge')->first())
                        <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                            <dt class="text-sm font-medium text-gray-500">
                                Challenger image
                            </dt>
                            <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                <img class="shadow overflow-hidden sm:rounded-lg"
                                     src="{{$challenge->getMedia('challenge')->first()->getUrl()}}">
                            </dd>
                        </div>
                    @endif

                    @if($challenge->status->getMedia('challenge_status')->first())
                        <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                            <dt class="text-sm font-medium text-gray-500">
                                Challenged picture
                            </dt>
                            <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                <img class="shadow overflow-hidden sm:rounded-lg"
                                     src="{{$challenge->status->getMedia('challenge_status')->first()->getUrl()}}">
                            </dd>
                        </div>
                    @endif

                    @if( $challenge->targets()->first()->user()->id == auth()->user()->id)
                        <form method="post" action="{{$update}}" enctype="multipart/form-data">
                            @method('POST')
                            @csrf

                            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <label for="status" class="block text-sm font-medium text-gray-700">Status</label>
                                <select id="status" name="status"
                                        class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                                    @foreach($statuses as $key => $status)
                                        <option @if(isset($challenge) && $key === $challenge->status()->first()->status_text_id) selected
                                                @endif value="{{ $key }}">{{ $status['text'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <label for="status" class="block text-sm font-medium text-gray-700">Upload image</label>
                                <div class="flex w-full bg-grey-lighter">
                                    <label class="w-64 flex flex-col items-center px-4 py-6 bg-white text-blue rounded-lg shadow-lg tracking-wide uppercase border border-blue cursor-pointer hover:bg-blue hover:text-white">
                                        <svg class="w-8 h-8" fill="currentColor" xmlns="http://www.w3.org/2000/svg"
                                             viewBox="0 0 20 20">
                                            <path d="M16.88 9.1A4 4 0 0 1 16 17H5a5 5 0 0 1-1-9.9V7a3 3 0 0 1 4.52-2.59A4.98 4.98 0 0 1 17 8c0 .38-.04.74-.12 1.1zM11 11h3l-4-4-4 4h3v3h2v-3z"/>
                                        </svg>
                                        <span class="mt-2 text-base leading-normal">Select a file</span>
                                        <input type='file' name="image" class="hidden"/>
                                    </label>
                                </div>
                            </div>
                            <div class="bg-gray-50 px-4 py-5 w-full">
                                <button
                                        class="inline-flex px-5 py-3 border border-transparent text-base font-medium rounded-md text-white bg-blue-900 hover:bg-blue-600">
                                    Save
                                </button>
                            </div>
                        </form>
                    @endif
                </dl>
            </div>
        </div>
    </div>
    <div class="mt-10 pt-10 flex lg:mt-0 lg:flex-shrink-0">
        <div class="inline-flex rounded-md shadow">
            <a href="{{url()->previous()}}"
               class="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-blue-900 bg-white hover:bg-blue-200">
                Back
            </a>
        </div>
        @if(auth()->user()->id == $challenge->user_id)
            <div class="ml-3 inline-flex rounded-md shadow">
                <a href="{{route('challenge.edit', ['challenge' => $challenge['id']])}}"
                   class="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-white bg-blue-900 hover:bg-blue-600">
                    Edit
                </a>
            </div>
        @endif
    </div>

</x-app-layout>
