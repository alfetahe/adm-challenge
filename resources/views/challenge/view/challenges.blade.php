<x-app-layout>
    <x-slot name="header">
        <h1 class="text-3xl font-extrabold tracking-tight text-white my-10">
            {{ __('Challenges') }}
        </h1>
    </x-slot>
    <div class="flex flex-col">
        <div class="overflow-x-auto">
            <div class="py-2 align-middle inline-block min-w-full">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                    <table class="min-w-full divide-y divide-gray-200">
                        <thead class="bg-gray-50">
                        <tr>
                            <th scope="col" colspan="1"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                {{ __('Challenge') }}
                            </th>
                            <th scope="col" colspan="1"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                {{ __('Short description') }}
                            </th>
                            <th scope="col" colspan="1"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                {{ __('Challenger') }}
                            </th>
                            <th scope="col" colspan="1"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                {{ __('Target/-s') }}
                            </th>
                            <th scope="col" colspan="1"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                {{ __('Status') }}
                            </th>
                        </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200" x-max="1">
                        @foreach($challenges as $challenge)
                            <tr>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <a href="{{route('challenge.show',['challenge' => $challenge['id']])}}">
                                        <div class="text-sm text-gray-900">{{$challenge['challenge_title']}}</div>
                                    </a>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm text-gray-900">{{ $challenge['challenge_description'] }}</div>
                                </td>
                                <td class="px-6 py-4">
                                    <div class="text-sm text-gray-900">{{ $challenge->challenger()->first()->name }}</div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm text-gray-900">{{ $challenge->targets()->first()->user()->name }}</div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                        <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-{{\App\Models\ChallengeStatus::STATUSES[$challenge->status()->first()->status_text_id]['color']}}-100 text-{{\App\Models\ChallengeStatus::STATUSES[$challenge->status()->first()->status_text_id]['color']}}-800">
                                            {{ \App\Models\ChallengeStatus::STATUSES[$challenge->status()->first()->status_text_id]['text'] }}
                                        </span>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="mt-10 pt-10 flex lg:mt-0 lg:flex-shrink-0">
            <div class="inline-flex rounded-md shadow">
                <a href="{{url()->previous()}}" class="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-blue-900 bg-white hover:bg-blue-200">
                    Back
                </a>
            </div>
            <div class="ml-3 inline-flex rounded-md shadow">
                <a href="{{route('challenge.create')}}" class="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-white bg-blue-900 hover:bg-blue-600">
                    Add new challenge
                </a>
            </div>
        </div>
    </div>
</x-app-layout>
