<x-app-layout>
    <x-slot name="header">
        <h1 class="text-3xl font-extrabold tracking-tight text-white my-10">
            {{ __('All You need is Challenge') }}
        </h1>
    </x-slot>
    <form method="POST" action="{{ $route }}" enctype="multipart/form-data">
        @csrf

        <div class="mt-10 sm:mt-0">
            <div class="md:grid md:grid-cols-4 md:gap-6">
                <div class="mt-5 md:mt-2 md:col-span-2">
                    <div class="shadow overflow-hidden sm:rounded-md">
                        <div class="px-4 py-5 bg-white sm:p-6">
                            <div class="grid grid-cols-6 gap-6">
                                <div class="col-span-12">
                                    <label for="email_address"
                                           class="block text-sm font-medium text-gray-700">Challenge name</label>
                                    <input type="text" name="challenge_title" id="challenge_title"
                                           value="{{ $challenge['challenge_title'] ?? '' }}"
                                           class="mt-1 block w-full shadow-sm text-lg border border-gray-300 rounded-md">
                                </div>

                                <div class="col-span-12">
                                    <label for="email_address"
                                           class="block text-sm font-medium text-gray-700">Challenge description</label>
                                    <textarea name="challenge_description" id="jk"
                                              class="mt-1 block w-full shadow-sm text-lg border border-gray-300 rounded-md"
                                    >{{ $challenge->challenge_description ?? '' }}</textarea>
                                </div>

                                <div class="col-span-12">
                                    <label for="targets" class="block text-sm font-medium text-gray-700">Challenge
                                        target</label>
                                    <select id="targets" name="targets"
                                            class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                                        @foreach($users as $user)
                                            <option @if(isset($challenge) && $user->id === $challenge->targets()->first()->user_id) selected @endif value="{{ $user->id }}">{{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-span-12 mb-8">
                                    <label for="status" class="block text-sm font-medium text-gray-700">Upload image</label>
                                    <div class="flex w-full bg-grey-lighter">
                                        <label class="w-64 flex flex-col items-center px-4 py-6 bg-white text-blue rounded-lg shadow-lg tracking-wide uppercase border border-blue cursor-pointer hover:bg-blue hover:text-white">
                                            <svg class="w-8 h-8" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                                <path d="M16.88 9.1A4 4 0 0 1 16 17H5a5 5 0 0 1-1-9.9V7a3 3 0 0 1 4.52-2.59A4.98 4.98 0 0 1 17 8c0 .38-.04.74-.12 1.1zM11 11h3l-4-4-4 4h3v3h2v-3z" />
                                            </svg>
                                            <span class="mt-2 text-base leading-normal">Select a file</span>
                                            <input type='file' name="image" class="hidden" />
                                        </label>
                                    </div>
                                </div>

                                </div>

                                <div class="col-span-12">
                                    <label for="status" class="block text-sm font-medium text-gray-700">Status</label>
                                    <select id="status" name="status"
                                            class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                                        @foreach($statuses as $key => $status)
                                            <option @if(isset($challenge) && $key === $challenge->status()->first()->status_text_id) selected @endif value="{{ $key }}">{{ $status['text'] }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-span-6">
                                    <label for="deadline"
                                           class="block text-sm font-medium text-gray-700">Deadline</label>
                                    <input type="date" name="deadline" id="deadline" value="{{ isset($challenge->deadline) ?
                                                      date('Y-m-d', strtotime($challenge->deadline)) :  date(time()) }}"
                                           class="mt-1 block w-full shadow-sm text-lg border border-gray-300 rounded-md">
                                </div>

                                <div class="col-span-6">
                                    <label for="bet" class="block text-sm font-medium text-gray-700">Bet</label>
                                    <input type="text" name="bet" id="bet"value="{{ $challenge['bet'] ?? '' }}"
                                           class="mt-1 block w-full shadow-sm text-lg border border-gray-300 rounded-md">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-10 pt-10 flex lg:mt-0 lg:flex-shrink-0">
            <div class="inline-flex rounded-md shadow">
                <a href="{{ route('challenges') }}" class="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-blue-900 bg-white hover:bg-blue-200">
                    Back
                </a>
            </div>
            <div class="ml-3 inline-flex rounded-md shadow">
                <button class="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-white bg-blue-900 hover:bg-blue-600">
                    Save
                </button>
            </div>
        </div>
    </form>


</x-app-layout>
