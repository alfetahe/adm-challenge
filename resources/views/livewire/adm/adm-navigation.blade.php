<div class="menu-dropdown" x-data="{ open: false }">

    <!-- Hamburger -->
    <div class="relative inline-block text-left">
        <div>
            <button @mouseover="open = !open"
                    @mouseout="open = !open"
                    type="button"
                    class="inline-flex justify-center w-full px-4 py-2 text-sm font-medium text-gray-700 hover:bg-gray-transparent focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500">
                <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="bars" role="img"
                     xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="c-svg">
                    <path fill="currentColor"
                          d="M16 132h416c8.837 0 16-7.163 16-16V76c0-8.837-7.163-16-16-16H16C7.163 60 0 67.163 0 76v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16zm0 160h416c8.837 0 16-7.163 16-16v-40c0-8.837-7.163-16-16-16H16c-8.837 0-16 7.163-16 16v40c0 8.837 7.163 16 16 16z"
                          class=""></path>
                </svg>
            </button>
        </div>
    </div>

    <div class="relative inline-block text-left">
        <div x-show="open"
             @mouseover="open = !open"
             @mouseout="open = !open"
             class="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 divide-y divide-gray-100 focus:outline-none"
             role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
            <div class="py-1" role="none">
                <a href="{{ route('about') }}"
                   class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                   role="menuitem"> {{__('About')}}</a>
                <a href="{{ route('dashboard') }}"
                   class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                   role="menuitem">{{ __('Dashboard') }}</a>
                <a href="{{ route('profile.show') }}"
                   class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                   role="menuitem">{{ __('Profile') }}</a>
                <a href="{{ route('challenges') }}"
                   class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                   role="menuitem"> {{__('Challenges')}}</a>
            </div>
            <div class="py-1" role="none">
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <a onclick="event.preventDefault();
                this.closest('form').submit();"
                       href="{{ route('logout') }}"
                       class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-900"
                       role="menuitem">{{ __('Log out') }}</a>
                </form>
            </div>
        </div>
    </div>
</div>
