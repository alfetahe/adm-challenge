<div class="flex-shrink-0 flex items-center">
    <div class="adm-logo"><a href="{{ route('dashboard') }}">
            <img src="{{url('/front/images/logo-international.svg')}}" alt="Logo"></a>
    </div>
</div>