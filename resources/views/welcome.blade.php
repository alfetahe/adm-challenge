<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
</head>
{{--      @if (Route::has('login'))
            <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                @auth
                    <a href="{{ url('/dashboard') }}" class="text-sm text-gray-700 underline">Dashboard</a>
                @else
                    <a href="{{ route('login') }}" class="text-sm text-gray-700 underline">Log in</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 underline">Register</a>
                    @endif
                @endauth
            </div>
        @endif--}}

<body class="body-bg min-h-screen pt-12 md:pt-20 pb-6 px-2 md:px-0">
<div class="adm-logo"><a href="#">
        <img src="{{url('/front/images/logo-international.svg')}}" alt="Logo"></a>
</div>
<main class="container mx-auto c-front-page">
    <section class="bg-white p-4 rounded shadow-2xl max-w-4xl mx-auto c-cta">
        <div
                class="mx-auto py-4 px-4 lg:flex lg:items-center lg:justify-between">
            <h2 class="text-3xl font-extrabold tracking-tight text-gray-900 sm:text-4xl mr-10">
                <span>All You need</span> <span class="text-blue-500"> is Challenge</span>
            </h2>
            <div class="mt-8 flex lg:mt-0 lg:flex-shrink-0">
                <div class="inline-flex rounded-md shadow">
                    <a href="{{route('challenges')}}" class="inline-flex items-center
                     justify-center px-5 py-3 border
                      border-transparent text-base
                       font-medium rounded-md
                        text-white bg-blue-500 hover:bg-blue-700">
                        Get started
                    </a>
                </div>
                @if(!Auth::check())
                    <div class="ml-3 inline-flex rounded-md shadow">
                        <a href="{{route('login.microsoft')}}" class="inline-flex items-center justify-center px-5 py-3
                        border border-transparent text-base font-medium rounded-md text-blue-500 bg-white hover:bg-blue-50">
                            Login Microsoft
                        </a>
                    </div>
                @endif
                @if(!Auth::check())
                    <div class="ml-3 inline-flex rounded-md shadow">
                        <a href="{{route('register')}}" class="inline-flex items-center justify-center px-5 py-3
                        border border-transparent text-base font-medium rounded-md text-blue-500 bg-white hover:bg-blue-50">
                            Register
                        </a>
                    </div>
                @endif
            </div>
        </div>
    </section>
    <div class="mt-5 flex content-center max-w-3xl mx-auto">
        <img src="{{url('/front/images/front.png')}}" alt="Challange">
    </div>
</main>
</body>
</html>
