<x-app-layout>
<x-slot name="header">
    <h1 class="text-3xl font-extrabold tracking-tight text-white my-10">
        {{ __('About ADM Challenges') }}
    </h1>
</x-slot>


    <div class="max-w-2xl">
        <div class="bg-white shadow overflow-hidden sm:rounded-lg">
            <div class="px-4 py-5 sm:px-6">
                <h3 class="text-lg leading-6 font-medium text-gray-900">
                    The Why
                </h3>
                <p class="mt-3 max-w-2xl text-sm text-gray-500 mb-10">
                    In short, to engage ADM employees with each other. The idea behind it occurred, when all ADM personal was sent to remote work due to COVID-19 pandemic and we were not able to go to our office. Thus, we were not able to engage with each other as we usually do (a bit of fun and silliness, the usual stuff). But, where there is a problem, there is always a solution. Luckily, we live in a digital era, where communication via online platforms is as simple as ever, so it was only natural that the communication thrived, and our Teams chats were flooded with notifications and orange dots displaying new message notifications.

                    So, in addition to chatting, how can we really engage with each other? The Ice Bucket Challenge brought together the entire world. Challenges! We started off with the challenges in Teams chats, but those 100 messages will quickly grow unmanageable. Creative minds and hard builders as we are, why not build an ADM internal challenges interface where it can be easily managed and visually presented. And so, we did! As part of a two-day hackathon, we sketched the idea and built it. Here we are.                  </p>
                <h3 class="text-lg leading-6 font-medium text-gray-900">
                    How it works?
                </h3>
                <p class="mt-3 max-w-2xl text-sm text-gray-500">
                    Simple! If you are an ADM employee, you can log in with your ADM Microsoft account and then you will land on an ADM Challenges dashboard where you can see the top challengers as challenge targets. You can easily navigate using the menu to browse the entire challengers-, targets- and challenges list and follow their statuses. But the main function and purpose! <strong>To add a challenge.</strong> Challenge a person or multiple persons or even an entire team! Describe the challenge, set a date, choose your targets and off we go! If you want to make things interesting, you can even set a price or a bet (a golden nugget so to speak) to keep the target/-s motivated. Target/-s then have to upload a picture to prove they have completed the challenge and collect their victory. If not, then accept their loss (shame).                  </p>
            </div>
        </div>
    </div>

    <div class="mt-10 pt-10 flex lg:mt-0 lg:flex-shrink-0">
        <div class="inline-flex rounded-md shadow">
            <a href="{{url()->previous()}}" class="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-blue-900 bg-white hover:bg-blue-200">
                Back
            </a>
        </div>
        <div class="ml-3 inline-flex rounded-md shadow">
            <a href="{{route('challenge.create')}}" class="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-white bg-blue-900 hover:bg-blue-600">
                Add new challenge
            </a>
        </div>
    </div>


</x-app-layout>
