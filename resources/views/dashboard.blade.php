<x-app-layout>
    <x-slot name="header">
        <h1 class="text-3xl font-extrabold tracking-tight text-white my-10">
            {{ __('Dashboard') }}
        </h1>
    </x-slot>


    <div class="grid grid-cols-2 gap-8">
        <div class="flex flex-col">
            <div class="overflow-x-auto">
                <h2 class="mb-3 text-2xl tracking-tight text-blue-200">Top challengers</h2>
                <div class="py-2 align-middle inline-block min-w-full">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                        <table class="min-w-full divide-y divide-gray-200">
                            <thead class="bg-gray-50">
                            <tr>
                                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Challenger
                                </th>
                                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Challenge count
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200" x-max="1">

                            @foreach($top_challengers as $top_challenger)
                                <tr>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="flex items-center">
                                            <div class="flex-shrink-0 h-10 w-10">
                                                <img class="h-10 w-10 rounded-full" src="/images/person-default.png" alt="person-img">
                                            </div>
                                            <div class="ml-4">
                                                <div class="text-sm font-medium text-gray-900">
                                                    {{ $top_challenger['challenger'] }}
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <div class="text-sm text-gray-900">{{ $top_challenger['challenges_count'] }}</div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
              {{--  <div class="text-right"><a href="#" class="my-4 text-white">View all challengers</a></div>--}}
            </div>
        </div>
        <div class="flex flex-col">
            <div class="overflow-x-auto">
                <h2 class="mb-3 text-2xl tracking-tight text-blue-200">Top targets</h2>
                <div class="py-2 align-middle inline-block min-w-full">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                        <table class="min-w-full divide-y divide-gray-200">
                            <thead class="bg-gray-50">
                            <tr>
                                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Challenge targets
                                </th>
                                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Challenge count
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200" x-max="1">

                                @foreach($top_targets as $top_target)
                                    <tr>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="flex items-center">
                                                <div class="flex-shrink-0 h-10 w-10">
                                                    <img class="h-10 w-10 rounded-full" src="/images/person-default.png" alt="person-img">
                                                </div>
                                                <div class="ml-4">
                                                    <div class="text-sm font-medium text-gray-900">
                                                        {{ $top_target['target'] }}
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="px-6 py-4 whitespace-nowrap">
                                            <div class="text-sm text-gray-900">{{ $top_target['target_challenges_count'] }}</div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
        {{--        <div class="text-right"><a href="#" class="my-4 text-white">View all targets</a></div>--}}
            </div>
        </div>
    </div>


    <div class="mt-10 pt-10 flex lg:mt-0 lg:flex-shrink-0">
        <div class="inline-flex rounded-md shadow">
            <a href="{{route('challenge.create')}}" class="inline-flex items-center justify-center px-5 py-3 border border-transparent text-base font-medium rounded-md text-white bg-blue-900 hover:bg-blue-600">
                Add new challenge
            </a>
        </div>
    </div>

</x-app-layout>
